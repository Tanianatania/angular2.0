﻿using AutoMapper;
using BLL.DTO;
using DAL.Entities;
using System.Collections.Generic;

namespace ProjectStructure.Profilies
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>();
            CreateMap<Project, ProjectDTO>();
            CreateMap<List<ProjectDTO>, List<Project>>();
            CreateMap<List<Project>, List<ProjectDTO>>();
        }
    }
}
