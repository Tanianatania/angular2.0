﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.DTO;
using BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ProjectStructure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskStateModelsController : ControllerBase
    {
        private readonly IService<TaskStateModelDTO> _service;

        public TaskStateModelsController(IService<TaskStateModelDTO> service)
        {
            _service = service;
        }

        // GET: api/TaskStateModels
        [HttpGet]
        public async Task<IEnumerable<TaskStateModelDTO>> Get()
        {
            return await _service.GetList();
        }

        // GET: api/TaskStateModels/5
        [HttpGet("{id}", Name = "GetTaskStateModel")]
        public async Task<TaskStateModelDTO> Get(int id)
        {
            return await _service.Get(id);
        }

        // POST: api/TaskStateModels
        [HttpPost]
        public async Task Post([FromBody] string value)
        {
            TaskStateModelDTO item = JsonConvert.DeserializeObject<TaskStateModelDTO>(value);
            await _service.Create(item);
        }

        // PUT: api/TaskStateModels
        [HttpPut]
        public async Task Put([FromBody] string value)
        {
            TaskStateModelDTO item = JsonConvert.DeserializeObject<TaskStateModelDTO>(value);
            await _service.Update(item);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
           await _service.Delete(id);
        }
    }
}
