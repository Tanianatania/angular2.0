﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worker.Models
{
    public class ResultModel
    {
        public string Message { get; set; }
        public DateTime Date { get; set; }
    }
}
