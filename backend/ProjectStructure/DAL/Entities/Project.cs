﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    public class Project
    {
        public int Id { get; set; }
        [MaxLength(10)]
        public string Name { get; set; }
        [MaxLength(50)]
        [MinLength(10)]
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public virtual User Author { get; set; }
        [Required]
        public int TeamId { get; set; }
        public virtual Team Team { get; set; }
        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, Description: {Description.Replace("\n", " ")}, Create at: {CreatedAt}, Deadline: {Deadline}, " +
                $"Author id: {AuthorId}, Team id: {TeamId}\n";
        }
    }
}
