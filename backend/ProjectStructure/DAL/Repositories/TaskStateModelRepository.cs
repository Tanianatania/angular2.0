﻿using System;
using System.Collections.Generic;
using System.Text;
using DAL.Interfaces;
using DAL.Entities;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public class TaskStateModelRepository : IRepository<TaskStateModel>
    {
        private readonly ApplicationDbContext _context;

        public TaskStateModelRepository(ApplicationDbContext context)
        {
            _context = context;
            _disposed = false;
        }

        public Task<List<TaskStateModel>> GetList()
        {
            return _context.TaskStateModels.ToListAsync();
        }

        public Task<TaskStateModel> Get(int id)
        {
            return _context.TaskStateModels.FirstOrDefaultAsync(a => a.Id == id);
        }

        public async Task Create(TaskStateModel item)
        {
            await _context.TaskStateModels.AddAsync(item);
        }

        public async Task Update(TaskStateModel item)
        {
            var state = await _context.TaskStateModels.FirstOrDefaultAsync(a => a.Id == item.Id);
            if (state != null)
            {
                 _context.TaskStateModels.Update(state);
            }
        }

        public async Task Delete(int id)
        {
            var state = await _context.TaskStateModels.FirstOrDefaultAsync(a => a.Id == id);
            if (state != null)
            {
                _context.TaskStateModels.Remove(state);
            }
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        #region Dispose pattern

        private bool _disposed;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
    }
}
