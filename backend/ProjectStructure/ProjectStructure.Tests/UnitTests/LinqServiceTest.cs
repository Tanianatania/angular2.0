﻿using AutoMapper;
using BLL.DTO;
using BLL.DTO.LinqModels;
using BLL.Hubs;
using BLL.Services;
using DAL.Entities;
using DAL.Interfaces;
using FakeItEasy;
using Microsoft.AspNetCore.SignalR;
using NUnit.Framework;
using SharedQueueService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Tests
{
    [TestFixture]
    public class LinqServiceTest
    {
        private LinqService service;

        [Test]
        [TestCase(1)]
        public async Task GetCountOfTaskInProjectByUserId_Should_return_count_of_tasks_in_project_when_given_project_id(int Id)
        {
            var elements = await service.GetCountOfTaskInProjectByUserId(Id);
            var result = elements.ToList();
            var expected = new CountOfTasksInProjectsDTO
            {
                Project = new ProjectDTO
                {
                    Id = 1,
                    Name = "Project 1",
                    Description = "It is project 1",
                    CreatedAt = DateTime.Now.AddDays(-12),
                    Deadline = DateTime.Now.AddDays(12),
                    AuthorId = 1,
                    TeamId = 1
                },
                CountOfTasks = 1
            };

            Assert.That(result[0].Project.Id, Is.EqualTo(expected.Project.Id));
            Assert.That(result[0].CountOfTasks, Is.EqualTo(expected.CountOfTasks));
        }

        [Test]
        [TestCase(1)]
        public async Task GetByProjectIdTest_Should_return_project_information_When_given_project_id(int Id)
        {
            var result = await service.GetByProjectId(Id);
            var expected = new ProjectInformationDTO
            {
                Project = new ProjectDTO
                {
                    Id = 1,
                    Name = "Project 1",
                    Description = "It is project 1",
                    CreatedAt = DateTime.Now.AddDays(-12),
                    Deadline = DateTime.Now.AddDays(12),
                    AuthorId = 1,
                    TeamId = 1
                },
                CountOfUser = 1,
                TaskWithLongestDescrition = new TasksDTO
                {
                    Id = 1,
                    Name = "Task 1",
                    State = 3,
                    CreatedAt = DateTime.Now.AddMonths(-2),
                    Description = "It is task 1",
                    FinishedAt = DateTime.Now.AddDays(100),
                    PerformerId = 1,
                    ProjectId = 1
                },
                TaskWithShortestName = new TasksDTO
                {
                    Id = 1,
                    Name = "Task 1",
                    State = 3,
                    CreatedAt = DateTime.Now.AddMonths(-2),
                    Description = "It is task 1",
                    FinishedAt = DateTime.Now.AddDays(100),
                    PerformerId = 1,
                    ProjectId = 1
                }
            };

            Assert.That(result.Project.Id, Is.EqualTo(expected.Project.Id));
            Assert.That(result.CountOfUser, Is.EqualTo(expected.CountOfUser));
            Assert.That(result.TaskWithLongestDescrition.Id, Is.EqualTo(expected.TaskWithLongestDescrition.Id));
            Assert.That(result.TaskWithShortestName.Id, Is.EqualTo(expected.TaskWithShortestName.Id));
        }

        [Test]
        [TestCase(1)]
        public async Task GetByUserIdTest_Should_return_user_information_When_given_user_id(int Id)
        {
            var element = await service.GetByUserId(Id);
            var expected = new UserInformationDTO
            {
                User = new UserDTO
                {
                    Id = 1,
                    FirstName = "First",
                    LastName = "Last",
                    Birthday = new DateTime(2011, 07, 28, 22, 35, 5),
                    Email = "user1@gmail.com",
                    RegisteredAt = new DateTime(2011, 07, 28, 22, 35, 5),
                    TeamId = 1
                },
                MinProject = new ProjectDTO {
                    Id = 2,
                    Name = "Project 2",
                    Description = "It is project 2",
                    CreatedAt = DateTime.Now,
                    Deadline = DateTime.Now.AddDays(123),
                    AuthorId = 1,
                    TeamId = 1
                },
                CountOfTaskInMinProject=1,
                CountOfCanceledOrNotFinishedTasks=0,
                LongestTask=new TasksDTO
                {
                    Id = 1,
                    Name = "Task 1",
                    State = 3,
                    CreatedAt = DateTime.Now.AddMonths(-2),
                    Description = "It is task 1",
                    FinishedAt = DateTime.Now.AddDays(100),
                    PerformerId = 1,
                    ProjectId = 1
                }
            };

            Assert.That(element.User.Id, Is.EqualTo(expected.User.Id));
            Assert.That(element.MinProject.Id, Is.EqualTo(expected.MinProject.Id));
            Assert.That(element.LongestTask.Id, Is.EqualTo(expected.LongestTask.Id));
            Assert.That(element.CountOfTaskInMinProject, Is.EqualTo(expected.CountOfTaskInMinProject));
            Assert.That(element.CountOfCanceledOrNotFinishedTasks, Is.EqualTo(expected.CountOfCanceledOrNotFinishedTasks));
        }

        [Test]
        [TestCase(1)]
        public async Task GetFinishedTasksTest_Should_return_list_of_tasks_When_given_id_existing_user(int Id)
        {
            var elem = await service.GetFinishedTasks(Id);
            var result = elem.ToList();
            var expected = new List<FinishedTasksDTO>
            {
                new FinishedTasksDTO
                {
                    Id=1,
                    Name="Task 1"
                },
            };

            Assert.That(result[0].Id, Is.EqualTo(expected[0].Id));
            Assert.That(result[0].Name, Is.EqualTo(expected[0].Name));
        }

        [Test]
        public async Task GetTeamWithYoungUserTest_Should_return_team_that_have_user_older_12_year_When_such_exists()
        {
            var elem = await service.GetTeamWithYoungUser();
            var expected = new TeamWithUserDTO
            {
                Id = 2,
                Name = "Team 2",
                Users = new List<UserDTO> { new UserDTO
                {
                    Id = 2,
                Birthday = new DateTime(1979, 07, 28, 22, 35, 5),
                Email = "user2@gmail.com",
                FirstName = "Name",
                LastName = "Last",
                RegisteredAt = new DateTime(1979, 07, 28, 22, 35, 5),
                TeamId = 2
                }
                }
            };
            var result = elem.ToArray()[0];

            Assert.That(expected.Id, Is.EqualTo(result.Id));
            Assert.That(expected.Name, Is.EqualTo(result.Name));
            Assert.That(expected.Users.Count, Is.EqualTo(result.Users.Count));
        }

        [Test]
        public async Task GetSortedListByUserAndTeamTest_Should_return_sorted_list_When_they_are_exists()
        {
            var elem = await service.GetSortedListByUserAndTeam();
            var result = elem.ToArray();
            var expected = new[]
            {
                new UserWithTasksDTO
                {
                    User=new UserDTO
                    {
                        Id = 1,
                FirstName = "First",
                LastName = "Last",
                Birthday = new DateTime(2011, 07, 28, 22, 35, 5),
                Email = "user1@gmail.com",
                RegisteredAt = new DateTime(2011, 07, 28, 22, 35, 5),
                TeamId = 1
                    },
                    Tasks=new List<TasksDTO>
                    {
                        new TasksDTO
            {
                Id = 1,
                Name = "Task 1",
                State = 3,
                CreatedAt = DateTime.Now.AddMonths(-2),
                Description = "It is task 1",
                FinishedAt = DateTime.Now.AddDays(100),
                PerformerId = 1,
                ProjectId = 1
            },
                        new TasksDTO
            {
                Id = 2,
                Name = "Task 2",
                State = 1,
                CreatedAt = DateTime.Now.AddMonths(-2),
                Description = "It is task 2",
                FinishedAt = DateTime.Now.AddDays(100),
                PerformerId = 1,
                ProjectId = 2
            }
                    }
                }
            };

            Assert.That(result.Count, Is.EqualTo(expected.Length));
            Assert.That(result[0].Tasks.Count, Is.EqualTo(expected[0].Tasks.Count()));
        }

        [Test]
        [TestCase(1)]
        public async Task GetTaskOfUser_Should_return_tasks_of_user_When_given_user_id_that_is_exists(int id)
        {
            var elements = await service.GetTaskOfUser(id);
            var result = elements.ToList();
            var expected = new List<TasksDTO>
            {
                new TasksDTO
            {
                Id = 1,
                Name = "Task 1",
                State = 3,
                CreatedAt = DateTime.Now.AddMonths(-2),
                Description = "It is task 1",
                FinishedAt = DateTime.Now.AddDays(100),
                PerformerId = 1,
                ProjectId = 1
            },
                new TasksDTO
            {
                Id = 2,
                Name = "Task 2",
                State = 1,
                CreatedAt = DateTime.Now.AddMonths(-2),
                Description = "It is task 2",
                FinishedAt = DateTime.Now.AddDays(100),
                PerformerId = 1,
                ProjectId = 2
            }
            };

            Assert.That(result.Count, Is.EqualTo(expected.Count));
            Assert.That(result[0].Id, Is.EqualTo(expected[0].Id));
            Assert.That(result[1].Id, Is.EqualTo(expected[1].Id));
        }

        [SetUp]
        public void  Service()
        {
            var project1 = new Project
            {
                Id = 1,
                Name = "Project 1",
                Description = "It is project 1",
                CreatedAt = DateTime.Now.AddDays(-12),
                Deadline = DateTime.Now.AddDays(12),
                AuthorId = 1,
                TeamId = 1
            };
            var project2 = new Project
            {
                Id = 2,
                Name = "Project 2",
                Description = "It is project 2",
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddDays(123),
                AuthorId = 1,
                TeamId = 1
            };
            var projectRepository = A.Fake<IRepository<Project>>();
            A.CallTo(() => projectRepository.GetList()).Returns(Task.FromResult(new[] { project1, project2 }.ToList()));

            var task1 = new Tasks
            {
                Id = 1,
                Name = "Task 1",
                State = 3,
                CreatedAt = DateTime.Now.AddMonths(-2),
                Description = "It is task 1",
                FinishedAt = DateTime.Now.AddDays(100),
                PerformerId = 1,
                ProjectId = 1
            };
            var task2 = new Tasks
            {
                Id = 2,
                Name = "Task 2",
                State = 1,
                CreatedAt = DateTime.Now.AddMonths(-2),
                Description = "It is task 2",
                FinishedAt = DateTime.Now.AddDays(100),
                PerformerId = 1,
                ProjectId = 2
            };
            var taskRepository = A.Fake<IRepository<Tasks>>();
            A.CallTo(() => taskRepository.GetList()).Returns(Task.FromResult(new[] { task1, task2 }.ToList()));

            var user1 = new User
            {
                Id = 2,
                Birthday = new DateTime(1979, 07, 28, 22, 35, 5),
                Email = "user2@gmail.com",
                FirstName = "Name",
                LastName = "Last",
                RegisteredAt = new DateTime(1979, 07, 28, 22, 35, 5),
                TeamId = 2
            };
            var user2 = new User
            {
                Id = 1,
                FirstName = "First",
                LastName = "Last",
                Birthday = new DateTime(2011, 07, 28, 22, 35, 5),
                Email = "user1@gmail.com",
                RegisteredAt = new DateTime(2011, 07, 28, 22, 35, 5),
                TeamId = 1
            };
            var userRepository = A.Fake<IRepository<User>>();
            A.CallTo(() => userRepository.GetList()).Returns(Task.FromResult(new[] { user1, user2 }.ToList()));

            var team1 = new Team
            {
                Id = 1,
                Name = "Team 1",
                CreatedAt = DateTime.Now.AddDays(-19)
            };
            var team2 = new Team
            {
                Id = 2,
                Name = "Team 2",
                CreatedAt = DateTime.Now.AddDays(19)
            };
            var teamRepository = A.Fake<IRepository<Team>>();
            A.CallTo(() => teamRepository.GetList()).Returns(Task.FromResult(new[] { team1, team2 }.ToList()));

            var state1 = new TaskStateModel
            {
                Id = 1,
                Value = "creaete"
            };
            var state2 = new TaskStateModel
            {
                Id = 2,
                Value = "canceled"
            };
            var state3 = new TaskStateModel
            {
                Id = 3,
                Value = "finished"
            };
            var stateRepository = A.Fake<IRepository<TaskStateModel>>();
            A.CallTo(() => stateRepository.GetList()).Returns(Task.FromResult(new[] { state1, state2, state3 }.ToList()));

            var messageProducerScopeFactory = A.Fake<IMessageProducerScopeFactory>();
            var messageConsumerScopeFactory = A.Fake<IMessageConsumerScopeFactory>();
            var hub = A.Fake<IHubContext<ProjectHub>>();

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<Team, TeamDTO>();
                cfg.CreateMap<Tasks, TasksDTO>();
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<Project, ProjectDTO>();
            });
            var mapper = mapperConfig.CreateMapper();

            service = new LinqService(projectRepository,
                taskRepository,
                userRepository,
                teamRepository,
                messageProducerScopeFactory,
                messageConsumerScopeFactory,
                hub,
                mapper);
        }
    }
}

