﻿using AutoMapper;
using BLL.DTO;
using BLL.Hubs;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using FakeItEasy;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SharedQueueService.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Tests
{
    public class TaskServiceTest
    {
        [Test]
        [TestCase(2)]
        public async Task ChangeStateByIdTest_Should_change_task_state_When_given_correct_task_id(int Id)
        {
            var service = SetUpService();
            var resultId = await service.ChangeStateById(Id);
            var changedTask = await service.Get(Id);

            Assert.That(resultId, Is.EqualTo(Id));
            Assert.That(changedTask.State, Is.EqualTo(3));

        }

        private TaskService SetUpService()
        {
            var task1 = new Tasks
            {
                Id = 1,
                Name = "Task 1",
                State = 3,
                CreatedAt = DateTime.Now.AddMonths(-2),
                Description = "It is task 1",
                FinishedAt = DateTime.Now.AddDays(100),
                PerformerId = 1,
                ProjectId = 1
            };
            var task2 = new Tasks
            {
                Id = 2,
                Name = "Task 2",
                State = 1,
                CreatedAt = DateTime.Now.AddMonths(-2),
                Description = "It is task 2",
                FinishedAt = DateTime.Now.AddDays(100),
                PerformerId = 1,
                ProjectId = 2
            };
            var taskRepository = A.Fake<IRepository<Tasks>>();
            A.CallTo(() => taskRepository.GetList()).Returns(Task.FromResult(new[] { task1, task2 }.ToList()));
            A.CallTo(() => taskRepository.Get(A<int>._)).Returns(Task.FromResult(task2));

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Tasks, TasksDTO>();
            });
            var mapper = mapperConfig.CreateMapper();
            var messageProducerScopeFactory = A.Fake<IMessageProducerScopeFactory>();
            var messageConsumerScopeFactory = A.Fake<IMessageConsumerScopeFactory>();
            var hub = A.Fake<IHubContext<ProjectHub>>();

            var svc = new TaskService(taskRepository,
                messageProducerScopeFactory,
                messageConsumerScopeFactory,
                hub,
                mapper);

            return svc;
        }
    }
}
