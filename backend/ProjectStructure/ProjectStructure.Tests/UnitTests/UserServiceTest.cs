﻿using AutoMapper;
using BLL.DTO;
using BLL.Hubs;
using BLL.Services;
using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories;
using FakeItEasy;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using SharedQueueService.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Tests
{
    public class UserServiceTest
    {
        [Test]
        [Obsolete]
        public async Task UserCreateTest_Should_create_new_user_When_given_correct_information()
        {
            var userRepository = A.Fake<IRepository<User>>();

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<UserDTO, User>();
            });
            var mapper = mapperConfig.CreateMapper();
            var messageProducerScopeFactory = A.Fake<IMessageProducerScopeFactory>();
            var messageConsumerScopeFactory = A.Fake<IMessageConsumerScopeFactory>();
            var hub = A.Fake<IHubContext<ProjectHub>>();

            var svc = new UserService(userRepository,
                messageProducerScopeFactory,
                messageConsumerScopeFactory,
                hub,
                mapper);

            var user = new UserDTO
            {
                Id = 0,
                Team = null,
                FirstName = "First",
                LastName = "Last",
                Birthday = new DateTime(2011, 07, 28, 22, 35, 5),
                Email = "user1@gmail.com",
                RegisteredAt = new DateTime(2011, 07, 28, 22, 35, 5),
                TeamId = 1
            };


            await svc.Create(user);
            A.CallTo(() => userRepository.Create(A<User>._)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => userRepository.Save()).MustHaveHappened(Repeated.Exactly.Once);
        }

        [Test]
        [Obsolete]
        public async Task ChangeTeamTest_Should_change_user_team_When_update_user()
        {
            var userRepository = A.Fake<IRepository<User>>();

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<User, UserDTO>();
                cfg.CreateMap<UserDTO, User>();
            });
            var mapper = mapperConfig.CreateMapper();
            var messageProducerScopeFactory = A.Fake<IMessageProducerScopeFactory>();
            var messageConsumerScopeFactory = A.Fake<IMessageConsumerScopeFactory>();
            var hub = A.Fake<IHubContext<ProjectHub>>();
            A.CallTo(() => userRepository.Get(A<int>._)).Returns(new User
            {
                Id = 0,
                Team = null,
                FirstName = "First",
                LastName = "Last",
                Birthday = new DateTime(2011, 07, 28, 22, 35, 5),
                Email = "user1@gmail.com",
                RegisteredAt = new DateTime(2011, 07, 28, 22, 35, 5),
                TeamId = 1
            });

            var svc = new UserService(userRepository,
                messageProducerScopeFactory,
                messageConsumerScopeFactory,
                hub,
                mapper);


            var newUser= new UserDTO
            {
                Id = 0,
                Team = null,
                FirstName = "First",
                LastName = "Last",
                Birthday = new DateTime(2011, 07, 28, 22, 35, 5),
                Email = "user1@gmail.com",
                RegisteredAt = new DateTime(2011, 07, 28, 22, 35, 5),
                TeamId = 2
            };


            await svc.Update(newUser);
            A.CallTo(() => userRepository.Update(A<User>._)).MustHaveHappened(Repeated.Exactly.Once);
            A.CallTo(() => userRepository.Save()).MustHaveHappened(Repeated.Exactly.Once);
        }

    }
}
