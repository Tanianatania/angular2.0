﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using BLL.DTO;
using System.Linq;
using SharedQueueService.Interfaces;
using Microsoft.AspNetCore.SignalR;
using BLL.Hubs;
using RabbitMQ.Client;
using SharedQueueService.Models;
using RabbitMQ.Client.Events;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TaskStateModelService : IService<TaskStateModelDTO>
    {
        private readonly IMapper _mapper;
        private readonly IRepository<TaskStateModel> _repository;
        IMessageProducerScope _messageProducerScope;
        IMessageConsumerScope _messageConsumerScope;
        private readonly IHubContext<ProjectHub> _projectHub;

        public TaskStateModelService(IRepository<TaskStateModel> repository,
            IMessageProducerScopeFactory messageProducerScopeFactory,
             IMessageConsumerScopeFactory messageConsumerScopeFactory,
             IHubContext<ProjectHub> projectHub,
            IMapper mapper)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSetting
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSetting
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
            _messageConsumerScope.MessageConsumer.Received += GetValue;

            _projectHub = projectHub;
            _repository = repository;
            _mapper = mapper;
        }
        private void GetValue(object sender, BasicDeliverEventArgs arg)
        {
            var value = Encoding.UTF8.GetString(arg.Body);
            _projectHub.Clients.All.SendAsync("GetNotification", value);
            _messageConsumerScope.MessageConsumer.SetActnowledge(arg.DeliveryTag, true);
        }

        public async Task Create(TaskStateModelDTO item)
        {
            _messageProducerScope.MessageProducer.Send("TaskState creating was triggered");
            var taskState = _mapper.Map<TaskStateModelDTO, TaskStateModel>(item);
            await _repository.Create(taskState);
            await _repository.Save();
        }

        public async Task Delete(int id)
        {
            _messageProducerScope.MessageProducer.Send("TaskState deleting was triggered");
            await _repository.Delete(id);
            await _repository.Save();
        }

        public async Task<TaskStateModelDTO> Get(int id)
        {
            _messageProducerScope.MessageProducer.Send("TaskState geting by Id was triggered");
            return _mapper.Map<TaskStateModel, TaskStateModelDTO>(await _repository.Get(id));
        }

        public async Task<IEnumerable<TaskStateModelDTO>> GetList()
        {
            _messageProducerScope.MessageProducer.Send("Loading all task states was triggered");
            return (await _repository.GetList()).Select(item => _mapper.Map<TaskStateModel, TaskStateModelDTO>(item));
        }

        public async Task Update(TaskStateModelDTO item)
        {
            _messageProducerScope.MessageProducer.Send("TaskState updating was triggered");
            var taskState = _mapper.Map<TaskStateModelDTO, TaskStateModel>(item);
            await _repository.Update(taskState);
            await _repository.Save();
        }
    }
}
