﻿using AutoMapper;
using DAL.Entities;
using DAL.Interfaces;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using BLL.DTO;
using System.Linq;
using SharedQueueService.Interfaces;
using SharedQueueService.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using Microsoft.AspNetCore.SignalR;
using BLL.Hubs;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Tasks> _repository;
        IMessageProducerScope _messageProducerScope;
        IMessageConsumerScope _messageConsumerScope;
        private readonly IHubContext<ProjectHub> _projectHub;

        public TaskService(IRepository<Tasks> repository,
            IMessageProducerScopeFactory messageProducerScopeFactory,
             IMessageConsumerScopeFactory messageConsumerScopeFactory,
             IHubContext<ProjectHub> projectHub,
            IMapper mapper)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSetting
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "SendValueQueue",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.Connect(new MessageScopeSetting
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "SendResponseQueue",
                RoutingKey = "response"
            });
            _messageConsumerScope.MessageConsumer.Received += GetValue;

            _projectHub = projectHub;
            _repository = repository;
            _mapper = mapper;
        }

        private void GetValue(object sender, BasicDeliverEventArgs arg)
        {
            var value = Encoding.UTF8.GetString(arg.Body);
            _projectHub.Clients.All.SendAsync("GetNotification", value);
            _messageConsumerScope.MessageConsumer.SetActnowledge(arg.DeliveryTag, true);
        }

        public async Task Create(TasksDTO item)
        {
            _messageProducerScope.MessageProducer.Send("Task creating was triggered");
            var task = _mapper.Map<TasksDTO, Tasks>(item);
            await _repository.Create(task);
            await _repository.Save();
        }

        public async Task Delete(int id)
        {
            _messageProducerScope.MessageProducer.Send("Task deleting was triggered");
            await _repository.Delete(id);
            await _repository.Save();
        }

        public async Task<TasksDTO> Get(int id)
        {
            _messageProducerScope.MessageProducer.Send("Task getting by Id was triggered");
            return _mapper.Map<Tasks, TasksDTO>(await _repository.Get(id));
        }

        public async Task<IEnumerable<TasksDTO>> GetList()
        {
            _messageProducerScope.MessageProducer.Send("Loading all tasks was triggered");
            return (await _repository.GetList()).Select(item => _mapper.Map<Tasks, TasksDTO>(item));
        }

        public async Task<int> ChangeStateById(int Id)
        {
            Tasks task = (await _repository.GetList()).Where(item => item.Id == Id).FirstOrDefault();
            if (task != null)
            {
                task.State = 3;
                await _repository.Update(task);
                await _repository.Save();
                return Id;
            }
            else
            {
                return -1;
            }
        }

        public async Task Update(TasksDTO item)
        {
            _messageProducerScope.MessageProducer.Send("Task updating was triggered");
            var task = _mapper.Map<TasksDTO, Tasks>(item);
            await _repository.Update(task);
            await _repository.Save();
        }
    }
}
